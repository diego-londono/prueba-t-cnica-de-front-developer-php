<!doctype html>
<html class="no-js">
        <head>
        <meta charset="utf-8">
        <title>Prueba Técnica de Front Developer PHP</title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1">

        <link rel="shortcut icon" href="images/dalo.jpg">
        
        {{ HTML::style('components/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('components/jquery-ui/jqueryui.css') }}
        {{ HTML::style('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') }}

        {{ HTML::style('css/style.css') }}

        {{ HTML::script('components/modernizr/modernizr.js') }}
        {{ HTML::script('components/prefixfree/prefixfree.min.js') }}

        <script> var site_url = "{{ URL::to('/') }}/"; </script>
    </head>
    <body>
        <header>
            <div class="wrapper-main">
                <div class="row">
                    <section class="menu-drop">
                        <i class="menu-drop fa fa-bars fa-3x" data-open="false"></i>
                    </section>
                    <nav class="col-md-12" id="main-menu">
                        <ul>
                            @foreach ( $menu as $parent )
                            <li class="inline">
                                <a class="block" id="item-menu-{{ $parent->id }}" href="javascript:;" >{{ $parent->name }}</a>
                                <?php $children = Menu::parent($parent->id)->state(1)->get(); ?>
                                @if ( $children )
                                <ul class="none sub">
                                    @foreach ( $children as $item )
                                        <li class="inline"><a class="block" href="javascript:;">{{ $item->name }}</a></li>
                                    @endforeach
                                </ul>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <section id="main">
            <div class="wrapper-main">
                <section id="posts">
                    <div class="row">
                        <article class="post-item col-md-4">
                            <div class="border-item">
                                <figure class="block">
                                    {{ HTML::image('images/dalo.jpg', '', [ 'class' => 'img-responsive center' ]) }}
                                </figure>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur ab, iusto consectetur voluptatum sunt eius maxime nulla obcaecati. Eius possimus fuga, quos iste tempore dolore culpa minima.</p>
                            </div>
                        </article>
                        <article class="post-item col-md-4">
                            <div class="border-item">
                                <figure class="block">
                                    {{ HTML::image('images/dalo.jpg', '', [ 'class' => 'img-responsive center' ]) }}
                                </figure>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur ab, iusto consectetur voluptatum sunt eius maxime nulla obcaecati. Eius possimus fuga, quos iste tempore dolore culpa minima. In, necessitatibus, praesentium.</p>
                            </div>
                        </article>
                        <article class="post-item col-md-4">
                            <div class="border-item">
                                <figure class="block">
                                    {{ HTML::image('images/dalo.jpg', '', [ 'class' => 'img-responsive center' ]) }}
                                </figure>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur ab, iusto consectetur voluptatum sunt eius maxime nulla obcaecati.</p>
                            </div>
                        </article>
                    </div>
                </section>
                <section id="information">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs inline" role="tablist">
                                <li class="active" role="presentation">
                                    <a href="#form" data-toggle="tab">Registro</a>
                                </li>
                                <li class="" role="presentation">
                                    <a href="#info" data-toggle="tab">Información</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <section class="tab-pane fade in active" rol="tabpanel" id="form">
                                    <form class="form-horizontal" id="form-register">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="full_name">Nombre Completo</label>
                                                <input class="form-control" id="full_name" name="full_name" type="text" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="age">Edad</label>
                                                <input class="form-control" id="age" name="age" type="number" required data-parsley-type="integer" data-parsley-length="[1,100]">
                                            </div>
                                            <div class="form-group">
                                                <label for="genre">Genero</label>
                                                <select class="form-control" id="genre" name="genre" required>
                                                    <option value="">Selecciona</option>
                                                    <option value="Femenino">Femenino</option>
                                                    <option value="Masculino">Masculino</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="date">Fecha Nacimiento</label>
                                                <input class="form-control" id="date" name="date" type="text" required placeholder="aaaa-mm-dd">
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary" id="btn-register" type="button">Enviar</button>
                                                <button class="btn btn-default" type="reset">Borrar</button>
                                            </div>
                                        </div>
                                    </form>
                                </section>
                                <section class="tab-pane fade in" rol="tabpanel" id="info">
                                    <article id="data">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    </article>
                                </section>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </section>
        <footer>
            <div class="wrapper-main">
                <div class="row">
                    <section class="col-md-12" id="copy">
                        <p class="inline"><strong>Copyright © 2015 Diego Londoño</strong> Todos los derechos reservados | Medellín - Colombia.</p>
                    </section>
                </div>
            </div>
        </footer>

        <section id="preload">
            <i class="fa fa-spinner fa-pulse fa-4x"></i>
        </section>

        <section id="loader">
            <i class="fa fa-refresh fa-spin fa-3x fa-fw margin-bottom"></i>
        </section>

        <section id="popup">
            <article class="block center">
                <div class="inline" id="message"></div>
            </article>
        </section>

        {{ HTML::script('components/jquery/jquery.min.js') }}
        {{ HTML::script('components/jquery/jquery-migrate.min.js') }}
        {{ HTML::script('components/jquery-ui/jqueryui.min.js') }}

        {{ HTML::script('components/bootstrap/js/bootstrap.min.js') }}

        {{ HTML::script('components/greensock/TweenMax.js') }}
        {{ HTML::script('components/greensock/EasePack.js') }}
        {{ HTML::script('components/greensock/CSSPlugin.js') }}

        {{ HTML::script('components/parsley/parsley.min.js') }}
        {{ HTML::script('components/parsley/es.js') }}

        {{ HTML::script('js/scripts.js') }}
    </body>
</html>







