<h1>Hola,</h1>
<p>La persona {{ $full_name }} se acaba de registrar en el portal con los sig. datos:</p>
<ul>
    <li><strong>Nombre Completo:</strong> {{ $full_name }}</li>
    <li><strong>Edad:</strong> {{ $age }}</li>
    <li><strong>Genero:</strong> {{ $genre }}</li>
    <li><strong>Fecha Nacimiento:</strong> {{ $date }}</li>
</ul>
<p>Gracias por su atención <br> Feliz día!</p>