<!doctype html>
<html class="no-js">
        <head>
        <meta charset="utf-8">
        <title>404 Demin World - Americanino </title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1">

        <link rel="shortcut icon" href="//www.americanino.com/arquivos/favicon.ico">

        {{ HTML::style('components/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') }}

        {{ HTML::style('css/app.css') }}

        {{ HTML::script('components/modernizr/modernizr.js') }}
        {{ HTML::script('components/prefixfree/prefixfree.min.js') }}

        <script>
            var site_url = "{{ URL::to('/') }}/";
        </script>
    </head>
    <body id="errors">
        <header>
            <div class="wrapper-main">
                <div class="row">
                    <figure class="col-md-2" id="logo">
                        <a href="{{ URL::route('home') }}" title="Americanino">{{ HTML::image('images/americanino-demin-world-logo.png', 'Americanino Logo', [ 'class' => 'img-responsive' ]) }}</a>
                    </figure>
                    <nav class="col-md-offset-3 col-md-7" id="main-menu">
                        <ul>
                            <li class="inline"><a href="{{ URL::route('home') }}" class="block">Home</a></li>
                            <li class="inline"><a href="{{ URL::route('denim.guide', ['mujer']) }}" class="">The Denim Guide</a></li>
                            <li class="inline"><a href="{{ URL::route('jeans.finder') }}" class="block">Jeans Finder</a></li>
                            <li class="inline"><a href="{{ URL::route('dictionary') }}" class="block">Dictionary</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <section id="error">
            <div class="wrapper-main">
                <h1 class="block">No podemos encontrar la página que estas buscando</h1>
                <a href="{{ URL::route('home') }}">Regresa al inicio</a>
            </div>
        </section>

        <section id="preload">
            <i class="fa fa-spinner fa-pulse fa-4x"></i>
        </section>

        {{ HTML::script('components/jquery/jquery.min.js') }}
        {{ HTML::script('components/jquery/jquery-migrate.min.js') }}

        {{ HTML::script('components/backstretch/backstretch.min.js') }}
        {{ HTML::script('components/greensock/TweenMax.js') }}
        {{ HTML::script('components/greensock/EasePack.js') }}
        {{ HTML::script('components/greensock/CSSPlugin.js') }}
        
        <script>
            $(function() {
                $('body').backstretch(site_url + 'images/banners/banner-01.jpg');
            });

            $(window).on('load', function() {
                TweenMax.to('#preload', 1, { css: { opacity: 0 }, autoAlpha: 0, delay: 1, ease: Linear.easeNone, onComplete: function() {
                    $('#preload').hide();

                    TweenMax.to('#logo', 0.5, { css: { opacity: 1, top: '0' }, autoAlpha: 1, delay: 0.1, onComplete: function() {
                        TweenMax.to('#main-menu', 0.7, { css: { right: '0', opacity: 1 }, autoAlpha: 1, ease: Expo.easeOut });
                        TweenMax.to('#error', 0.7, { css: { opacity: 1 }, autoAlpha: 1 });
                    } });
                } });
            });
        </script>
    </body>
</html>
