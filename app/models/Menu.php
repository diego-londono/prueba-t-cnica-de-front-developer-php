<?php 

class Menu extends Eloquent {
    protected $table = 'menu';

    public function scopeParent($query, $id = null)
    {
        return empty($id) ? $query->whereNull('parent_id') : $query->whereParentId($id);
    }

    public function scopeState($query, $value)
    {
        return $query->whereState($value);
    }
}