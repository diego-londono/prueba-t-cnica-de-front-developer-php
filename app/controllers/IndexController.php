<?php

class IndexController extends BaseController {

    public function index()
    {
        $menu = Menu::parent()->state(1)->get();
        return View::make('index', [ 'menu' => $menu ]);
    }

    public function register()
    {
        if ( Request::ajax() ) {
            try {
                $validator = Validator::make( Input::all(), [
                        'full_name' => 'required',
                        'age'       => 'required',
                        'genre'     => 'required',
                        'date'      => 'required'
                    ] );

                if ( $validator->fails() )
                    throw new Exception('Por favor registra todos los campos.');

                $register               = new Register;
                $register->full_name    = Input::get('full_name');
                $register->age          = Input::get('age');
                $register->genre        = Input::get('genre');
                $register->date         = Input::get('date');

                if ( !$register->save() )
                    throw new Exception('Se presento un error al guardar los datos.');

                $data =  [
                        'full_name' => Input::get('full_name'),
                        'age'       => Input::get('age'),
                        'genre'     => Input::get('genre'),
                        'date'      => Input::get('date'),
                    ];
                    
                Mail::send( 'mail', $data, function($msg) {
                    $msg->to( 'jgantiva@chefcompany.co', Input::get('full_name') )
                        ->subject( 'Bienvenid@ a Chef Company' );
                } );

                return Response::json('Registro exitoso!', 200);
            } catch (Exception $e) {
                return Response::json($e->getMessage(), 400);
            }
        }
    }
}
