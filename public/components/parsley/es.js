// ParsleyConfig definition if not already set
window.ParsleyConfig            = window.ParsleyConfig || {};
window.ParsleyConfig.i18n       = window.ParsleyConfig.i18n || {};

window.ParsleyConfig.i18n.es    = $.extend(window.ParsleyConfig.i18n.es || {}, {
    defaultMessage: "Este campo parece ser inválido.",
    type: {
            email:        "Debes ingresar un correo válido.",
            url:          "Debes ingresar una URL válida.",
            number:       "Debes ingresar un número válido.",
            integer:      "Debes ingresar un número válido.",
            digits:       "Debes ingresar un dígito válido.",
            alphanum:     "Debes ingresar un dato alfanumérico."
        },
    notblank:       "Este campo no debe estar en blanco.",
    required:       "Este campo es requerido.",
    pattern:        "Este campo es incorrecto.",
    min:            "Ingresa un valor mínimo de %s dígitos.",
    max:            "Ingresa un valor máximo de %s dígitos.",
    range:          "Este campo debe estar entre %s y %s.",
    minlength:      "Este valor es muy corto. Ingresa uno de mínimo %s caracteres.",
    maxlength:      "Este valor es muy largo. Ingresa uno de máximo %s caracteres.",
    length:         "La longitud de este campo debe estar entre %s y %s caracteres.",
    mincheck:       "Debe seleccionar al menos %s opciones.",
    maxcheck:       "Debe seleccionar %s opciones o menos.",
    rangecheck:     "Debe seleccionar entre %s y %s opciones.",
    equalto:        "Este campo debe ser idéntico."
});

// If file is loaded after Parsley main file, auto-load locale
if ('undefined' !== typeof window.ParsleyValidator)
  window.ParsleyValidator.addCatalog('es', window.ParsleyConfig.i18n.es, true);
