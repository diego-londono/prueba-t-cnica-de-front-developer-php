
$(window).on('load', onLoadPage);
$(document).on('ready', onReadyPage);

function onReadyPage() {
    var iPad         = /ipad/i.test( navigator.userAgent.toLowerCase() );
    $.browser.device = ( /android|webos|iphone|ipod|blackberry|iemobile|opera mini/i.test( navigator.userAgent.toLowerCase() ) );

    if ( $.browser.device ) {
        jQuery('.menu-drop').on('click', function(e) {
            var $data = $(e.currentTarget);

            if ( $data.data('open') ) {
                TweenMax.to('#main-menu', 0.5, { css: { opacity: 0, display: 'none' }, autoAlpha: 0, delay: 0.1, ease: Linear.easeNone });
                $data.data('open', false).removeClass('fa-times').addClass('fa-bars');
            } else {
                TweenMax.to('#main-menu', 0.8, { css: { opacity: 1, display: 'block' }, autoAlpha: 1, delay: 0.1, ease: Linear.easeNone });
                $data.data('open', true).removeClass('fa-bars').addClass('fa-times');
            }
        });
    }

    $('#btn-register').on('click', onRegister);

    $('#date').datepicker( {
        changeMonth     : true,
        changeYear      : true,
        dateFormat      : "yy-mm-dd",
        yearRange       : "-95:-18",
        defaultDate     : "1975-01-01"
    } );

    $("#popup").on('click', function() {
        popup(false, '');
    });
}

function onRegister(e) {
    if ( $('#form-register').parsley().validate() ) {
        $.ajax({
            cache   : false,
            data    : $('#form-register').serializeArray(),
            dataType: 'json',
            type    : 'POST',
            url     : site_url + 'register',
            beforeSend: function() {
                $('#btn-register').off('click', onRegister);
                loader(true);
            } 
        })
        .complete(function() {
            $('#btn-register').on('click', onRegister);
            loader(false);
        })
        .done(function(data) {
            popup(true, data);
            $('#form-register').trigger("reset");
        })
        .error(function(response) {
            popup(true, response.responseText);
        });
    } else
        popup(true, 'Por favor registra todos los campos.');
}

function onLoadPage() {
    TweenMax.to('#preload', 1, { css: { opacity: 0, display: 'none' }, autoAlpha: 0, delay: 1, ease: Linear.easeNone, onComplete: function() {
        TweenMax.to('#main-menu', 0.7, { css: { right: '0', opacity: 1 }, autoAlpha: 1, ease: Expo.easeOut });
        TweenMax.to('footer', 0.7, { css: { bottom: '0', opacity: 1 }, autoAlpha: 1, ease: Linear.easeNone, onComplete: function() {
            TweenMax.to('#main', 0.6, { css: { opacity: 1 }, autoAlpha: 1 });
        } });
    } });
}

function loader(e) {
    if (e) 
        TweenMax.to('#loader', 0.3, { css: { opacity: 1, display: 'block' }, autoAlpha: 1, delay: 0.1, ease: Linear.easeNone });
    else
        TweenMax.to('#loader', 0.8, { css: { opacity: 0, display: 'none' }, autoAlpha: 0, delay: 0.1, ease: Linear.easeNone });
}

function popup(e, text) {
    if (e) {
        var top = ( $(window).height() - $('#popup article').height() ) * .5;
        TweenMax.to('#popup', 0.8, { css: { opacity: 1, display: 'block', 'padding-top': top }, autoAlpha: 1, delay: 0.1, ease: Linear.easeNone });
    }
    else 
        TweenMax.to('#popup', 0.5, { css: { opacity: 0, display: 'none', 'padding-top': 0 }, autoAlpha: 0, delay: 0.1, ease: Linear.easeNone });

    $('#popup #message').empty().text(text);
}